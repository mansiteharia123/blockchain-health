/**
 * New script file
 */
/**
 * Track the trade of a commodity from one trader to another
 * @param { org.mansiricha.health.Confirm} confirm
 * @transaction
 */
async function confirmAppointment(confirm) { // eslint-disable-line no-unused-vars
    if (confirm.appointment.status == "CONFIRMED") {
        throw new Error('Appointment already confirmed');
    }
    if (confirm.appointment.status === 'CONSULTED') {
        throw new Error('Appointment already consulted');
    }
    if (confirm.appointment.status === 'REJECTED') {
        throw new Error('Appointment already rejected');
    }

    confirm.appointment.status = "Confirmed";
    confirm.appointment.doctor = confirm.doctor;
    const assetRegistry = await getAssetRegistry('org.mansiricha.health.Appointment');
    const confirmNotification = getFactory().newEvent('org.mansiricha.health', 'ConfirmNotification');
    confirmNotification.appoint = confirm.appointment;
    emit(confirmNotification);
    await assetRegistry.update(confirm.appointment);

    confirm.patient.status = 'CONFIRMED';
    const patientRegistry = await getParticipantRegistry('org.mansiricha.health.Patient');
    await patientRegistry.update(confirm.patient);
}

/**
 * @param {  org.mansiricha.health.BuyMed } med
 * @transaction
 */

async function gettingMedicine(med) {
    name = 'R_00' + Math.floor(100000 + Math.random() * 900000);
    const receipt = getFactory().newResource('org.mansiricha.health', 'Receipt', name);

    receipt.providerID = med.chemist.cId;
    receipt.providedTo = med.patient.pId;
    receipt.amountPaid = med.chemist.costPermg;
    receipt.logTime = med.timestamp;
    const receiptRegistry = await getAssetRegistry('org.mansiricha.health.Receipt');
    await receiptRegistry.addAll([receipt]);
    med.patient.debt = med.patient.debt + med.chemist.costPermg;
    const patientRegistry = await getParticipantRegistry('org.mansiricha.health.Patient');
    await patientRegistry.update(med.patient);
}

/**
 * Transaction for recording appointment between doctor and patient.
 * @param { org.mansiricha.health.Consult } atx The sample transaction instance.
 * @transaction
 */
async function consult(atx) {
    if (atx.presc.isMedPrescribed && atx.presc.isTestPrescribed) {
        throw new Error('Prescription can not be both test and medicine')
    }
    if (!(atx.presc.isMedPrescribed || atx.presc.isTestPrescribed)) {
        throw new Error('Prescription should prescibe either test or medicine')
    }
    if (atx.appoint.status === 'PENDING') {
        throw new Error('Appointment yet to be  confirmed!')
    }
    if (atx.appoint.aId != atx.presc.appoint.aId) {
        throw new Error('Prescription not matching with the appointment!')
    }
    if (atx.appoint.status === 'CONSULTED') {
        throw new Error('Appointment already consulted!')
    }
    if (atx.appoint.status === 'REJECTED') {
        throw new Error('Appointment already rejected!')
    }
    if (atx.patient.pId != atx.appoint.patient.pId) {
        throw new Error('Patient Proxy not allowed!')
    }
    if (atx.presc.appoint.doctor.dId != atx.doctor.dId) {
        throw new Error('You are not the designated doctor')
    }
    const appointRegistry = await getAssetRegistry('org.mansiricha.health.Appointment');
    atx.appoint.status = 'CONSULTED'
    await appointRegistry.update(atx.appoint);

    const presRegistry = await getAssetRegistry('org.mansiricha.health.Prescription');
    atx.presc.appoint = atx.appoint
    await presRegistry.update(atx.presc);
}


/**
 * Transaction for buying test from pathalogy lab.
 * @param {org.mansiricha.health.BuyTest} atx The sample transaction instance.
 * @transaction
 */
async function BuyTest(atx) {
    if (atx.presc.isMedPrescribed && atx.presc.isTestPrescribed) {
        throw new Error('Prescription can not be both test and medicine')
    }
    if (!(atx.presc.isMedPrescribed || atx.presc.isTestPrescribed)) {
        throw new Error('Prescription should prescibe either test or medicine')
    }
    if (atx.presc.isMedPrescribed) {
        throw new Error('Prescription should prescibe test only')
    }
    if (atx.patient.pId != atx.presc.patient.pId) {
        throw new Error('This prescription is intended to someone else')
    }
    if (atx.patient.pId != atx.presc.appoint.insuranceId.patient.pId) {
        throw new Error('Insurance Id is not matching with the patient')
    }

    let cost = 0.0
    for (let i in atx.presc.test) {
        let index = atx.pathlab.test.indexOf(atx.presc.test[i])
        if (index < 0) {
            throw new Error('Not Allowed')
        }
        cost += atx.pathlab.cost[index]
    }
    if (atx.presc.appoint.isInsured) {
        if (cost <= atx.presc.appoint.insuranceId.ensuredAmount) {
            atx.presc.appoint.insuranceId.ensuredAmount -= cost
            const appointRegistry = await getAssetRegistry('org.mansiricha.health.Insurance');
            await appointRegistry.update(atx.presc.appoint.insuranceId);
        } else {
            throw new Error("Insurance doesn't exist")
        }
    } else {
        atx.patient.debt += cost
        const patientRegistry = await getParticipantRegistry('org.mansiricha.health.Patient');
        await patientRegistry.update(atx.patient)
    }
}