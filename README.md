# blockchain-health

Before you start

This tutorial will walk you through building a Hyperledger Composer Blockchain
solution from scratch.
In the space of a few hours you will be able to go from an idea for a disruptive blockchain innovation, to executing transactions against a real Hyperledger Fabric blockchain network. The following steps are for use in the provided virtual machine. If you plan on going through the demo on your own machine with your own installation of Hyperledger, please make the proper changes in the path names and variable names.
The commands you will need to execute in the command line.

Steps for System Setup [NOT REQUIRED if you are using the VM]

Hyperledger prerequisites for Ubuntu
curl -O https://hyperledger.github.io/composer/unstable/prereqs-ubuntu.sh
chmod u+x prereqs-ubuntu.sh
./prereqs-ubuntu.sh

Composer prerequisites for Ubuntu

npm install -g composer-cli

npm install -g composer-rest-server

npm install -g generator-hyperledger-composer

npm install -g yo

npm install -g composer-playground


Fabric Installation (Every bullet is a single command)

● mkdir ~/fabric-dev-servers && cd ~/fabric-dev-servers

● curl -O https://raw.githubusercontent.com/hyperledger/composer-tools/master/packages/fabric-dev-servers/fabric-dev-servers.tar.gz

● tar -xvf fabric-dev-servers.tar.gz

● ./downloadFabric.sh


Docker Permission

sudo usermod -a -G docker $USER

sudo reboot

Starting fabric


Change directory into fabric-dev-servers by executing the following in the terminal

cd /home/hyperledger/fabric-dev-servers

Start fabric

./startFabric.sh

Create credentials

./createPeerAdminCard.sh


Setting up your business network
To use the yo tool, execute the following commands in the terminal.
cd ~/

yo hyperledger-composer:businessnetwork

Set up the network as follows

Business network name: blockchain-health

Description: nm

Author name: <Your name>

Author email: <Your email>

License: Apache-2.0

Namespace: org.mansiricha.health

Do you want to generate an empty template network?: No

Files will now be available in blockchain-health

cd /home/hyperledger/blockchain-health

As developers, the files important to us are in models, lib and permissions.acl

Defining the Business Network
Make the following changes to the files. To make the changes, you can either use a text editor like Atom/gedit or visual studio code to open the files, or you can open the files through the command line in nano by going to their location using cd. nano is a text editor for Unix systems that uses the command line
interface. While it gets the job done, and while many programmers swear by it, the
interface might not be as intuitive as any GUI based text editor you may have
experience using.

The commands for nano will be given below. You do not need to use them if you
choose to use Atom/gedit instead.

First, we will change the org.mansiricha.health.cto file.

The location of the file is /home/hyp.blockchain-health/models.

If you are using Atom/gedit, you can right-click on the file and open it with
Atom/gedit.

If are using nano ,execute the following commands in the terminal:

cd /home/hyp.blockchain-health/models

nano org.blockchain-health.cto


Replace all the contents of org.mansiricha.health.cto with the following cto file code.

Save the file directly if working in Atom/gedit .
If working in nano , follow the following steps:

Save: Ctrl+O

Exit: Ctrl+X

Now, we will change the script.js file in the models folder.

The location of the file is /home/hyperledger/blockchain-health/lib.

If you are using Atom/gedit, you can right-click on the file and open it with
Atom/gedit.
If are using nano , execute the following commands in the terminal:

cd /home/hyperledger/blockchain-health/lib
nano script.js

Replace all the contents of script.js with the lib/script.js file.


Save the file directly if working in Atom/gedit .
If working in nano , follow the following steps:
Save: Ctrl+O

Exit: Ctrl+X

Now, we will change the permissions.acl file in the models folder.

The location of the file is /home/hyperledger/blockchain-health.

If you are using Atom/gedit, you can right-click on the file and open it with
Atom/gedit.
If are using nano , execute the following commands in the terminal:
cd /home/hyperledger/blockchain-health

nano permissions.acl

Replace all the contents of permissions.acl with the following acl file


Save the file directly if working in Atom/gedit .
If working in nano , follow the following steps:
Save: Ctrl+O

Exit: Ctrl+X

Generating the Business Network Archive file. Now that we have created the artefacts required for the project, we can now create the archive that can be deployed on the fabric. In the terminal execute
cd /home/hyperledger/blockchain-health
composer archive create -t dir -n .
Deploying Business Network
We can now install the archive on the fabric. Execute the following commands in the terminal.
IMPORTANT: The following commands are supposed to be on one line per bullet.
If they show up as two different lines in the CLI, the command will not work as
one line will execute before the other and will return some error. If that happens,
please copy one line and paste the next line right next to it.

● cd /home/hyperledger/blockchain-health

● composer network install --card PeerAdmin@hlfv1 --archiveFile

blockchain-health@0.0.1.bna

● composer network start --networkName blockchain-health --networkVersion 0.0.1

--networkAdmin admin --networkAdminEnrollSecret adminpw --card

PeerAdmin@hlfv1 --file networkadmin.card

● composer card import --file networkadmin.card

● composer network ping --card admin@blockchain-health

Generate REST Server

We will now create a REST server. In the terminal, execute composer-rest-server. Choose the following options/Enter the following data when prompted.

Enter the name of the business network card to use: admin@blockchain-health

Specify if you want namespaces in the generated REST API: never use namespaces

Specify if you want to use an API key to secure the REST API: N

Specify if you want to enable authentication for the REST API using pa
ssport: N

Specify if you want to enable event publication over WebSockets: Y
Specify if you want to enable TLS security for the REST API: N

The REST server will now be installed.

Do not close this terminal window.

Now, go to your browser and open up http://localhost/3000
